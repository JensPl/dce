<?php

namespace ArminVieweg\Dce\ViewHelpers;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapFactory;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;

/**

add to TS-SETUP:

#remove your container elements from editPanel rendering, no otherway at this time
#!!! editpanel = 0 is default because with 1 as default dont work!!!
tt_content {
  stdWrap {
    editPanel = 0
    editPanel.override = 1
    editPanel.override.if {
      isInList.field = CType
      value = dce_dceuid1,dce_dceuid5,dce_dceuid11,dce_dceuid12,dce_dceuid13,dce_dceuid14,dce_dceuid15,dce_dceuid16,dce_dceuid21,dce_dceuid22
      negate = 1
    }
  }
}
 *
 * Display the editpanel for a given entity
 *
 * show panel and wrap content
 * <dce:editPanel for="dce|contentObject|tablename:uid"> Content for normal and edit wrap output <dce:editPanel>
 *
 * show link to edit content
 * <dce:editPanel for="dce|contentObject|tablename:uid" onlyForEdit="1" > klick here for edit <dce:editPanel>
 *
 * reduce buttons to show
 * <dce:editPanel for="dce|contentObject|tablename:uid" allow="{edit:1, move:1}" > klick here for edit <dce:editPanel>
 *
 */
class EditPanelViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
	/**
	 * @param AbstractEntity|array $for
     * @param integer $onlyForEdit [optional]
     * @param array $configuration [optional]
     * @param array $allow [optional]
     * @param string $onlyEdit [optional]
	 * @return string
	 */
	public function render($for, $onlyForEdit=0, array $configuration = ["label"=>"%s"], array $allow = ["edit"=>1, "move"=>1, "hide"=>1, "show"=>1,"new"=>1,"delete"=>1])
	{
		$content = $this->renderChildren();

		if (!$this->getBackendUserAuthentication()) {
			if ($onlyForEdit) return '';
			return $content;
		}

		if ( gettype($for) == "object" ) {
			$tableName = $this->getTableName($for);
			$uid = $for->getUid();
		}else if ( gettype($for) == "array" ) {
			$tableName = "tt_content";
			$uid = $for["uid"];
		}else if ( gettype($for) == "string" ) {
			list($tableName, $uid) = explode(":",$for);
		}

		if (!$tableName || !$uid) {
			return $content."EditPanel - render ERROR: got no Table nor uid ($tableName:$uid) to load for edit";
		}

		/* add Hook for extensions with own content render*/
		if ( $tableName == "tx_dce_domain_model_dce"){
			$tableName = "tt_content";
			$uid = $for->getContentObject()["uid"];
		}

		$row = $this->getDatabaseConnection()->exec_SELECTgetSingleRow('*', $tableName, 'uid=' . $uid);

		$configuration = $this->getAdminPanelConfiguration($configuration);

		$oFeEdit = new \TYPO3\CMS\Feedit\FrontendEditPanel;
		return $oFeEdit->editPanel( $content, $configuration, $tableName.':'.$uid, $row, $tableName, $allow );
	}

    /**
     * @param array $overrides [optional]
     * @return array
     */
    protected function getAdminPanelConfiguration(array $overrides = []) {
        $configuration = isset($this->getBackendUserAuthentication()->userTS['admPanel.'])
        	? $this->getBackendUserAuthentication()->userTS['admPanel.']
        	: [];
        ArrayUtility::mergeRecursiveWithOverrule($configuration, $overrides);
        return $configuration;
    }

	/**
	 * @param AbstractEntity $for
	 * @return string
	 */
	protected function getTableName(AbstractEntity $for)
	{
		$dataMapFactory = GeneralUtility::makeInstance(DataMapFactory::class);
		return $dataMapFactory->buildDataMap(get_class($for))->getTableName();
	}

	/**
	 * @return DatabaseConnection
	 */
	protected function getDatabaseConnection()
	{
		return $GLOBALS['TYPO3_DB'];
	}

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

}